import os
import numpy as np
import timeit
import json

def diagonal(n1,n2,pt):
    if(n1 == n2):
        return pt['MATCH']
    else:
        return pt['MISMATCH']

def pointers(di,ho,ve):

    pointer = max(di,ho,ve) #based on python default maximum(return the first element).

    if(di == pointer):
        return 'D'
    elif(ho == pointer):
        return 'H'
    else:
         return 'V'

def align(s1, s2, match=1, mismatch=-1, gap=-1):
    penalty = {'MATCH': match, 'MISMATCH': mismatch, 'GAP': gap}  # Penalty dictionary
    n = len(s1) + 1  # # matrix columns
    m = len(s2) + 1  # # matrix rows
    al_mat = np.zeros((m, n), dtype=int)  # Initializes the alignment matrix with zeros
    p_mat = np.zeros((m, n), dtype=str)  # Initializes the alignment matrix with zeros
    # Scans all the first rows element in the matrix and fill it with "gap penalty"
    for i in range(m):
        al_mat[i][0] = penalty['GAP'] * i
        p_mat[i][0] = 'V'
    # Scans all the first columns element in the matrix and fill it with "gap penalty"
    for j in range(n):
        al_mat[0][j] = penalty['GAP'] * j
        p_mat[0][j] = 'H'
    # Fill the matrix with the correct values.

    # Approximation constant to limit and optimize matrix area; assumes a maximum misalignment distance of
    # Ignoring this loop can give a more accurate results, yet computation time will become quadratic
    k = 2*abs(m-n)+20
    for i in range(1, m):
        for j in range(max([i-k,1]), min([i+k, n])):
            al_mat[i][j] = -k

    p_mat[0][0] = 0  # Return the first element of the pointer matrix back to 0.
    for i in range(1, m):
        for j in range(max([i-k,1]), min([i+k, n])):#range(1,n):#
            di = al_mat[i - 1][j - 1] + diagonal(s1[j - 1], s2[i - 1],
                                                 penalty)  # The value for match/mismatch -  diagonal.
            ho = al_mat[i][j - 1] + penalty['GAP']  # The value for gap - horizontal.(from the left cell)
            ve = al_mat[i - 1][j] + penalty['GAP']  # The value for gap - vertical.(from the upper cell)
            al_mat[i][j] = max(di, ho, ve)  # Fill the matrix with the maximal value.(based on the python default maximum)
            p_mat[i][j] = pointers(di, ho, ve)

    #helper indexer function for word statistics
    def getWordIndex(index):
        a = index
        while a >= 0 and not s1[a].isspace():
            a -= 1
        return a+1

    errors = []
    i = n-1
    j = m-1
    while i != 0 and j != 0:
        val = p_mat[j][i]

        if val == 'D' and al_mat[j-1][i-1] <= al_mat[j][i]: # if match
            i -= 1
            j -= 1
        else: #there is a discrepancy
            correctString = ''
            incorrectString = ''
            while (i != 0 and j != 0) and not (val == 'D' and al_mat[j-1][i-1] <= al_mat[j][i]):
                if val == 'D' and not al_mat[j-1][i-1] <= al_mat[j][i]: #mistmach
                    i -= 1
                    j -= 1
                    correctString = s1[i] + correctString
                    incorrectString = s2[j] + incorrectString
                elif val == 'V':
                    j -= 1
                    incorrectString = s2[j] + incorrectString
                elif val == 'H':
                    i -= 1
                    correctString = s1[i] + correctString
                else:
                    break
                #update value
                val = p_mat[j][i]
            wordIndex = -1
            if correctString != ' ': wordIndex = getWordIndex(i)
            errors.append([correctString, incorrectString, wordIndex])

    #format: [[correct, incorrect, word index],...])
    return errors


#Run
start = timeit.default_timer()

#directories
pathToCorrected = os.getcwd() + '/corrected'
pathToOriginal = os.getcwd() + '/original'

#filter out and convert the names of corrected files
correctedFileNames = [name for name in os.listdir(pathToCorrected) if name.endswith('.txt')]
originalFileNames = [name[2:] for name in correctedFileNames]

errorList = []
wordCount = 0
singleErrorWordCount = 0
multipleErrorWordCount = 0
charCount = 0
errorCharCount = 0
correctCharCountDict = {}
print('Comparing files...')

# Iterate to compare each file pair
for i in range(0,len(originalFileNames)):#1):#
    correctedFile = open(pathToCorrected + '/' + correctedFileNames[i], 'r')
    originalFile = open(pathToOriginal + '/' + originalFileNames[i], 'r')
    correctedText = correctedFile.read() #file text
    originalText = originalFile.read() #file text
    print(originalFileNames[i])

    newErrors = align(correctedText,originalText)

    charCount += len(''.join(correctedText.split()))
    errorCharCount += len(newErrors)

    # Get error word counts
    wordCount += len(correctedText.split())
    seen = []
    marked = []
    for _,_,index in newErrors:
        if index == -1: pass
        elif not index in seen:
            seen.append(index)
        elif index in seen and not index in marked:
            marked.append(index)
    multipleErrorWordCount += len(marked)
    singleErrorWordCount += len(seen) - len(marked)
    print(newErrors)
    errorList += newErrors

    #Get correct char counts
    for char in correctedText:
        if char in correctCharCountDict:
            correctCharCountDict[char] += 1
        else:
            correctCharCountDict[char] = 1

print('% Characters with error', errorCharCount / charCount * 100,'%')
print('% Words with one error:', singleErrorWordCount/ wordCount * 100,'%')
print('% Words with multiple errors:', multipleErrorWordCount/ wordCount * 100,'%')

# Count the occurrences of errors, to calculate probabilities later
confusionCountDictionary = {}
for char, mistake, _ in errorList:
    if char in confusionCountDictionary:
        if mistake in confusionCountDictionary[char]:
            confusionCountDictionary[char][mistake] += 1
        else:
            confusionCountDictionary[char][mistake] = 1
    else:
        confusionCountDictionary[char] = {mistake : 1}

# Add counts of correct chars to the dictionary
for char in confusionCountDictionary:
    if len(char) == 1:
        total = sum(confusionCountDictionary[char].values())
        confusionCountDictionary[char][char] = correctCharCountDict[char] - total

# Convert number of occurrences to probabilities
confusionProbsDict = confusionCountDictionary
for char in confusionCountDictionary:
    total = sum(confusionCountDictionary[char].values())
    for mistake in confusionCountDictionary[char]:
        confusionProbsDict[char][mistake] = confusionCountDictionary[char][mistake] / total

print(confusionProbsDict)

#Write confusion probabilities data to text file
filename = 'confusionProbabilities.txt'
print('Writing to file', filename)
with open(filename, 'w') as file:
    file.write(json.dumps(confusionProbsDict))

stop = timeit.default_timer()
print('\nCompleted in', stop - start, 'seconds.')



